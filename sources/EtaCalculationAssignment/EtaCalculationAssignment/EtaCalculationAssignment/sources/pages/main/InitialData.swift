//
//  InitialData.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class InitialData {
    static func addresses() -> [Address] {
        return [
            Address(address: "1 Apple Park Way, Cupertino, CA, USA",
                    coordinates: MapPoint(x: 37.33348755919196, y: -122.01021789778444)),
            Address(address: "Hollywood, Los Angeles, CA, USA",
                    coordinates: MapPoint(x: 34.088702853088606, y: -118.33265803971726)),
        ]
    }
}
