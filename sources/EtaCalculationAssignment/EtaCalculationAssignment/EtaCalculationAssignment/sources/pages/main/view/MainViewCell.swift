//
//  MainViewCell.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/31/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import UIKit

final class MainViewCell: UICollectionViewCell {
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        
        return label
    }()
    
    let coordinatesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        
        return label
    }()
    
    let etaLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        
        return label
    }()
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            self.nameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            self.coordinatesLabel.leadingAnchor.constraint(equalTo: self.nameLabel.leadingAnchor, constant: 150),
            self.etaLabel.leadingAnchor.constraint(equalTo: self.coordinatesLabel.leadingAnchor, constant: 150),
            self.etaLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            //
            self.nameLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.coordinatesLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.etaLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            //
            self.nameLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 130),
            self.coordinatesLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 150)
        ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.nameLabel)
        self.addSubview(self.coordinatesLabel)
        self.addSubview(self.etaLabel)
        self.backgroundColor = UIColor(red: 0.8078, green: 0.6353, blue: 0.5412, alpha: 1.0)
        
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
