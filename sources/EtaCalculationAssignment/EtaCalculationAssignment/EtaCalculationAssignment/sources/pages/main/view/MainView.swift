//
//  MainView.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import UIKit

final class MainView: UIView {
    
    static let cellId = "cellId"
    
    let collectionView = MainCollectionView()
    let resetButton: CustomButton = {
        let button = CustomButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Reset", for: .normal)
        
        return button
    }()
    
    private func setupConstraints() {
        guard let superview = self.superview else { return }
        
        NSLayoutConstraint.activate([
            self.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: superview.trailingAnchor),
            self.topAnchor.constraint(equalTo: superview.topAnchor),
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor),
            //
            collectionView.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: superview.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: superview.topAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: 800),
            //
            resetButton.topAnchor.constraint(greaterThanOrEqualTo: collectionView.bottomAnchor, constant: 10),
            resetButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
            resetButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20)
        ])
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.setupConstraints()
    }
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(collectionView)
        self.addSubview(resetButton)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
