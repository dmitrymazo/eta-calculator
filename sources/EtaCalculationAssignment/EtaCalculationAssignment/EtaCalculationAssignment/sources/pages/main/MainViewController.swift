//
//  MainViewController.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/27/21.
//

import UIKit

final class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDragDelegate, UICollectionViewDropDelegate, SearchAddressViewControllerDelegate {
    
    private let viewModel = MainViewModel()
    private let contentView = MainView()
    
    private let disposeBag = DisposeBag()
    
    private func bind() {
        viewModel.addresses.observeWithoutInitialCall { [weak self] addresses in
            DispatchQueue.main.async {
                self?.contentView.collectionView.reloadData()
            }
        }.addToBag(self.disposeBag)
    }
    
    private static func coordinatesTextFormat(for point: MapPoint) -> String {
        let numberOfPlaces = 8
        return "\(point.x.rounded(toPlaces: numberOfPlaces)), \(point.y.rounded(toPlaces: numberOfPlaces))"
    }
    
    private static func etaTextFormat(for time: TimeInterval) -> String {
        let timeString = String(format: "%.2f", time)
        return "\(timeString) hours"
    }
    
    @objc
    private func addButtonTapped() {
        let viewController = SearchAddressViewController()
        viewController.delegate = self
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc
    private func resetButtonTapped() {
        viewModel.reset()
    }
    
    private func item(for indexPath: IndexPath) -> AddressViewModel {
        return viewModel.addresses.value[indexPath.row]
    }
    
    private func removeItem(at indexPath: IndexPath) {
        viewModel.addresses.value.remove(at: indexPath.row)
    }
    
    private func insertItem(_ address: AddressViewModel, at indexPath: IndexPath) {
        viewModel.addresses.value.insert(
            address,
            at: indexPath.row)
    }
    
    // MARK: - Init
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = .white
        self.view.addSubview(self.contentView)
        self.bind()
        
        contentView.collectionView.delegate = self
        contentView.collectionView.dataSource = self
        contentView.collectionView.dragInteractionEnabled = true
        contentView.collectionView.dragDelegate = self
        contentView.collectionView.dropDelegate = self
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addButtonTapped))
        
        contentView.resetButton.addTarget(self, action: #selector(resetButtonTapped), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Internal
    
    func addToList(address: Address) {
        viewModel.addToList(address: address)
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.addresses.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainView.cellId, for: indexPath) as? MainViewCell else {
            return UICollectionViewCell()
        }
        
        let address = self.viewModel.addresses.value[indexPath.row]
        cell.nameLabel.text = address.address.address
        cell.coordinatesLabel.text = Self.coordinatesTextFormat(for: address.address.coordinates)
        cell.etaLabel.text = Self.etaTextFormat(for: address.eta)
        
        return cell
    }
    
    func indexPathForPreferredFocusedView(in collectionView: UICollectionView) -> IndexPath? {
        return IndexPath(row: 0, section: 0)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 130)
    }
    
    // MARK: - UICollectionViewDragDelegate
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let item = viewModel.addresses.value[indexPath.row].address.address
        let provider = NSItemProvider(object: item as NSString)
        let dragItem = UIDragItem(itemProvider: provider)
        
        return [dragItem]
    }
    
    // MARK: - UICollectionViewDropDelegate
    
    func collectionView(
        _ collectionView: UICollectionView,
        canHandle session: UIDropSession
    ) -> Bool {
        return true
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        dropSessionDidUpdate session: UIDropSession,
        withDestinationIndexPath destinationIndexPath: IndexPath?
    ) -> UICollectionViewDropProposal {
        return UICollectionViewDropProposal(
            operation: .move,
            intent: .insertAtDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        
        guard let destinationIndexPath = coordinator.destinationIndexPath else {
            return
        }
        
        coordinator.items.forEach { dropItem in
            guard let sourceIndexPath = dropItem.sourceIndexPath else {
                return
            }
            
            collectionView.performBatchUpdates({
                
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destinationIndexPath])
                
                var addresses = self.viewModel.addresses.value
                
                let itemAtSourcePath = addresses[sourceIndexPath.row]
                let itemAtDestinationPath = addresses[destinationIndexPath.row]
                
                addresses[sourceIndexPath.row] = itemAtDestinationPath
                addresses[destinationIndexPath.row] = itemAtSourcePath
                
                self.viewModel.addresses.value = addresses
            }, completion: { [unowned self] _ in
                
                let addresses = self.viewModel.addresses.value
                let modifiedAddresses = addresses.map { $0.address }
                
                self.viewModel.replaceAddresses(with: modifiedAddresses)
                
                coordinator.drop(
                    dropItem.dragItem,
                    toItemAt: destinationIndexPath)
            })
        }
    }
    
}
