//
//  MainViewModel.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/27/21.
//

import Foundation

struct MapPoint: Equatable {
    let x: Double
    let y: Double
}

enum GeolocationServiceError: Error {
    case loadingError(Error)
    case unsupportedString
    case parsingError
    case generalError
}

struct Address: Equatable {
    let address: String
    let coordinates: MapPoint
}

typealias GeolocationServiceCompletion = (Result<Address, GeolocationServiceError>) -> Void

protocol GeolocationService {
    func coordinates(for string: String, completion: @escaping GeolocationServiceCompletion)
}

final class MainViewModel {
    
    private(set) var addresses = Observable([AddressViewModel]())
    
    func addToList(address: Address) {
        let addressViewModel = AddressViewModelCreator.addressViewModel(from: address, previousViewModel: addresses.value.last)
        addresses.value.append(addressViewModel)
    }
    
    func replaceAddresses(with addresses: [Address]) {
        self.addresses.value = AddressViewModelCreator.addressViewModels(from: addresses)
    }
    
    func reset() {
        let addresses = InitialData.addresses()
        self.addresses.value = AddressViewModelCreator.addressViewModels(from: addresses)
    }
    
    init() {
        self.reset()
    }
}

final class AddressViewModel {
    let address: Address
    let eta: TimeInterval
    
    init(address: Address, eta: TimeInterval) {
        self.address = address
        self.eta = eta
    }
}
