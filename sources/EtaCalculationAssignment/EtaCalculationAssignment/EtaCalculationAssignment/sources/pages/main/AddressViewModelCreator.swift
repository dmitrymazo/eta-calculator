//
//  AddressViewModelCreator.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class AddressViewModelCreator {
    static func addressViewModel(from address: Address, previousViewModel: AddressViewModel?) -> AddressViewModel {
        guard let previousViewModel = previousViewModel else {
            return AddressViewModel(address: address,
                                    eta: 0)
        }
        
        let startPoint = previousViewModel.address.coordinates
        let previousEta = previousViewModel.eta
        
        let distanceInKm = ArialDistanceCalculator.distanceInKmBetweenEarthCoordinates(start: startPoint, end: address.coordinates)
        let eta = EtaCalculator.eta(distanceInKm: distanceInKm) + previousEta
        
        return AddressViewModel(address: address,
                                eta: eta)
    }
    
    static func addressViewModels(from addresses: [Address]) -> [AddressViewModel] {
        var viewModels = [AddressViewModel]()
        
        for i in 0..<addresses.count {
            let previousViewModel = viewModels[safe: i - 1]
            let address = addresses[i]
            let viewModel = Self.addressViewModel(from: address, previousViewModel: previousViewModel)
            
            viewModels.append(viewModel)
        }
        return viewModels
    }
}
