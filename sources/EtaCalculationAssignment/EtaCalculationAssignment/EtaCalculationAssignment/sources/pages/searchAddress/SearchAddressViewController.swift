//
//  SearchAddressViewController.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/27/21.
//

import UIKit

final class SearchAddressViewController: UIViewController, UITextFieldDelegate {
    
    /// Use WebGeolocationService to real data and FakeGeolocationService for simulation.
    private let viewModel = SearchAddressPageViewModel(coordinateFetcher: WebGeolocationService())
    private let contentView = SearchAddressPageView()
    
    private let disposeBag = DisposeBag()
    
    private var address: Address? {
        didSet {
            if let address = address {
                self.contentView.searchResultsView.addressLabel.text = "Address: \(address.address)"
                self.contentView.searchResultsView.latitudeLabel.text = "Latitude: \(address.coordinates.x)"
                self.contentView.searchResultsView.longitudeLabel.text = "Longitude: \(address.coordinates.y)"
            } else {
                self.contentView.searchResultsView.addressLabel.text = ""
                self.contentView.searchResultsView.latitudeLabel.text = ""
                self.contentView.searchResultsView.longitudeLabel.text = ""
            }
        }
    }
    
    weak var delegate: SearchAddressViewControllerDelegate?
    
    // MARK: - Private
    
    private func fetchAddress(from string: String) {
        viewModel.fetchAddress(from: string) { [weak self] result in
            result.onSuccess { address in
                DispatchQueue.main.async {
                    self?.address = address
                }
            }.onFail { error in
                // handle errors
            }
        }
    }
    
    @objc
    private func searchButtonTapped() {
        guard let text = self.contentView.textField.text else { return }
        self.fetchAddress(from: text)
    }
    
    @objc
    private func cancelButtonTapped() {
        self.dismissViewController()
    }
    
    @objc
    private func addButtonTapped() {
        guard let address = self.address else { return }
        self.delegate?.addToList(address: address)
        self.dismissViewController()
    }
    
    private func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Internal
    
    func textFieldShouldReturn(_ sender: UITextField) -> Bool {
        // User finished typing (hit return): hide the keyboard.
        sender.resignFirstResponder()
        return true
    }
    
    // MARK: - Init
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = .white
        self.view.addSubview(self.contentView)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButtonTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addButtonTapped))
        contentView.searchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        contentView.textField.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

protocol SearchAddressViewControllerDelegate: AnyObject {
    func addToList(address: Address)
}
