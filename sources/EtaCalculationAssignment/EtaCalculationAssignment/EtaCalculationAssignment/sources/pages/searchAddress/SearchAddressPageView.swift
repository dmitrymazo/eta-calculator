//
//  SearchAddressPageView.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/31/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import UIKit

final class SearchAddressPageView: UIView {
    
    let textField: UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        view.textColor = .black
        view.layer.cornerRadius = 3.0
        
        return view
    }()
    
    let searchButton: CustomButton = {
        let button = CustomButton()
        button.setTitle("Search", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    let searchResultsView = SearchResultsView()
    
    // MARK: - Internal
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.setupConstraints()
    }
    
    // MARK: - Private
    
    private func setupConstraints() {
        guard let superview = self.superview else { return }
        
        NSLayoutConstraint.activate([
            self.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: superview.trailingAnchor),
            self.topAnchor.constraint(equalTo: superview.topAnchor),
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor),
            //
            textField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            textField.topAnchor.constraint(equalTo: self.topAnchor, constant: 100),
            textField.widthAnchor.constraint(equalToConstant: 200),
            textField.heightAnchor.constraint(equalToConstant: 70),
            //
            searchButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            searchButton.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 15),
            //
            searchResultsView.topAnchor.constraint(equalTo: searchButton.bottomAnchor),
            searchResultsView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            searchResultsView.widthAnchor.constraint(equalToConstant: 200),
            searchResultsView.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .white
        self.addSubview(textField)
        self.addSubview(searchButton)
        self.addSubview(searchResultsView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
