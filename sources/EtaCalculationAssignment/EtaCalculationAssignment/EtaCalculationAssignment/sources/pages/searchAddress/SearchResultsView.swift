//
//  SearchResultsView.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/31/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import UIKit

final class SearchResultsView: UIView {
    
    let addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        
        return label
    }()
    
    let latitudeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        
        return label
    }()
    
    let longitudeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        
        return label
    }()
    
    // MARK: - Private
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            addressLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            addressLabel.topAnchor.constraint(equalTo: self.topAnchor),
            //
            latitudeLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            latitudeLabel.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: 20),
            //
            longitudeLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            longitudeLabel.topAnchor.constraint(equalTo: latitudeLabel.bottomAnchor, constant: 20)
        ])
    }
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(addressLabel)
        self.addSubview(latitudeLabel)
        self.addSubview(longitudeLabel)
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
