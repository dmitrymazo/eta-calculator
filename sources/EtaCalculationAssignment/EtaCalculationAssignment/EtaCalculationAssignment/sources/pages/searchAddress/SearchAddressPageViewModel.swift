//
//  SearchAddressPageViewModel.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/31/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class SearchAddressPageViewModel {
    
    private let coordinateFetcher: GeolocationService
    
    func fetchAddress(from string: String, completion: @escaping GeolocationServiceCompletion) {
        coordinateFetcher.coordinates(for: string) { result in
            result.onSuccess { address in
                completion(.success(address))
            }.onFail { error in
                completion(.failure(.loadingError(error)))
            }
        }
    }
    
    init(coordinateFetcher: GeolocationService) {
        self.coordinateFetcher = coordinateFetcher
    }
    
}
