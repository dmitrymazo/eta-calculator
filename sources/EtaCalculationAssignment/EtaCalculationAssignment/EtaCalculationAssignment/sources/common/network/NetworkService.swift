//
//  NetworkService.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class NetworkService {
    private var task: URLSessionTask?
    
    enum NetworkError: Error {
        case errorLoadingUrl(Error?)
    }
    
    typealias CoordinateFetcherCompletion = (Result<Data, NetworkError>) -> Void
    
    func request(with url: URL, completion: @escaping CoordinateFetcherCompletion) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                completion(.failure(.errorLoadingUrl(error)))
                return
            }
            completion(.success(data))
        }
        self.task = task
        task.resume()
    }
    
    func cancelAllRequests() {
        task?.cancel()
    }
}
