//
//  ArialDistanceCalculator.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class ArialDistanceCalculator {
    
    private static func degreesToRadians(degrees: Double) -> Double {
        return degrees * Double.pi / 180
    }
    
    static func distanceInKmBetweenEarthCoordinates(start: MapPoint, end: MapPoint) -> Double {
        let earthRadiusKm = 6371.0
        
        let deltaLatitude = Self.degreesToRadians(degrees: (end.x - start.x))
        let deltaLongitude = Self.degreesToRadians(degrees: (end.y - start.y))
        
        let startXRadians = Self.degreesToRadians(degrees: start.x)
        let endXRadians = Self.degreesToRadians(degrees: end.x)
        
        let a = sin(deltaLatitude/2) * sin(deltaLatitude/2) +
            sin(deltaLongitude/2) * sin(deltaLongitude/2) * cos(startXRadians) * cos(endXRadians)
        let c = 2 * atan2(sqrt(a), sqrt(1 - a))
        
        return earthRadiusKm * c
    }
}
