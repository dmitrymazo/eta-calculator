//
//  EtaCalculator.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class EtaCalculator {
    /// Time interval in hours corresponding to the distance in km.
    static func eta(distanceInKm distance: Double) -> TimeInterval {
        return distance * 0.02777778
    }
}
