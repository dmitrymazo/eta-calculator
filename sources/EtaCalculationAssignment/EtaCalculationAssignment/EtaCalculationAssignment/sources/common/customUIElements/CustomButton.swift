//
//  CustomButton.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/31/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import UIKit

final class CustomButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 3
        self.backgroundColor = UIColor(red: 0.5765, green: 0.3647, blue: 0, alpha: 1.0)
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.black.cgColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
