//
//  FakeGeolocationService.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class FakeGeolocationService: GeolocationService {
    func coordinates(for string: String, completion: @escaping GeolocationServiceCompletion) {
        guard string.count >= 4 else {
            completion(.failure(.unsupportedString))
            return
        }
        let x = (0...50).randomElement() ?? 0
        let y = (0...50).randomElement() ?? 0
        let point = MapPoint(x: Double(x), y: Double(y))
        let address = Address(address: string,
                              coordinates: point)
        
        completion(.success(address))
    }
}
