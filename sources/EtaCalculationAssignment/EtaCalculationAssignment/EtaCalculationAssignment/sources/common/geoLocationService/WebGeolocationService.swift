//
//  WebGeolocationService.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/30/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class WebGeolocationService: GeolocationService {
    private static let minStringLength = 4
    private let service = NetworkService()
    
    func coordinates(for string: String, completion: @escaping GeolocationServiceCompletion) {
        guard string.count >= Self.minStringLength
              , let encodedString = string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
              , let url = GeocodingApi.url(forEncodedString: encodedString) else {
            completion(.failure(.unsupportedString))
            return
        }
        
        service.request(with: url) { result in
            result.onSuccess { data in
                let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? Dictionary<String, Any>
                let results = json?["results"] as? [Dictionary<String, Any>]
                let firstResult = results?.first
                let location = firstResult?["location"] as? Dictionary<String, Double>
                
                guard let latitude = location?["lat"]
                      , let longitude = location?["lng"]
                      , let addressString = firstResult?["formatted_address"] as? String else {
                    completion(.failure(.parsingError))
                    return
                }
                
                let address = Address(address: addressString,
                                      coordinates: MapPoint(x: latitude, y: longitude))
                
                completion(.success(address))
            }.onFail { error in
                completion(.failure(.generalError))
            }
        }
    }
}

private final class GeocodingApi {
    private static let apiKey = "d6239d1c3c16268163c1c6ad18c261a9a87689c"
    
    static func url(forEncodedString string: String) -> URL? {
        return URL(string: "https://api.geocod.io/v1.6/geocode?q=\(string)&api_key=\(Self.apiKey)")
    }
}

private struct Response: Decodable {
    let results: [GeoResult]
}

private struct GeoResult: Decodable {
    let lat: Double
    let lng: Double
}
