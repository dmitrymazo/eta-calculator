//
//  Extensions.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/27/21.
//

import Foundation

extension Result {
    
    @discardableResult
    func onFail(_ f: (Failure) -> Void) -> Result {
        if case .failure(let error) = self {
            f(error)
        }
        return self
    }
    
    @discardableResult
    func onSuccess(_ f: (Success) -> Void) -> Result {
        if case .success(let val) = self {
            f(val)
        }
        return self
    }
    
}

extension Array {
    
    subscript(safe index: Index) -> Element? {
        return (startIndex..<endIndex) ~= index ? self[index] : nil
    }
    
}

extension Double {
    
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
}
