//
//  Observable.swift
//  EtaCalculationAssignment
//
//  Created by Dmitry Mazo on 8/27/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import Foundation

final class Observable<Value>: Disposable {
    
    typealias ClosureType = (Value) -> Void
    
    private var closures = [ClosureType]()
    private let queue = DispatchQueue(label: "Observable-\(UUID().uuidString)")
    
    var value: Value {
        didSet {
            self.queue.sync {
                self.closures.forEach { $0(self.value) }
            }
        }
    }
    
    init(_ value: Value) {
        self.value = value
    }
    
    func observe(_ closure: @escaping ClosureType) -> Self {
        self.queue.sync {
            self.closures.append(closure)
        }
        closure(self.value)
        return self
    }
    
    func observeWithoutInitialCall(_ closure: @escaping ClosureType) -> Self {
        self.queue.sync {
            self.closures.append(closure)
        }
        return self
    }
    
    func addToBag(_ disposeBag: DisposeBag) {
        disposeBag.add(self)
    }
    
    func dispose() {
        self.closures = []
    }
}

final class DisposeBag {
    private var observables = [Disposable]()
    
    func add(_ observable: Disposable) {
        self.observables.append(observable)
    }
    
    private func dispose() {
        self.observables.forEach { $0.dispose() }
    }
    
    deinit {
        self.dispose()
    }
}

protocol Disposable {
    func dispose()
}
