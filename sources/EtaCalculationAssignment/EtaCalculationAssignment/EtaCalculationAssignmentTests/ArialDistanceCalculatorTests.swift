//
//  ArialDistanceCalculatorTests.swift
//  EtaCalculationAssignmentTests
//
//  Created by Dmitry Mazo on 8/27/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import XCTest
@testable
import EtaCalculationAssignment

final class ArialDistanceCalculatorTests: XCTestCase {
    
    func testCalculateDistance1() {
        let start = MapPoint(x: 45.05716233204525, y: 7.659898081500805)
        let end = MapPoint(x: 40.860617807644864, y: 14.27574866911252)
        let distance = ArialDistanceCalculator.distanceInKmBetweenEarthCoordinates(start: start, end: end).rounded(toPlaces: 2)
        
        XCTAssertLessThan(distance, 712.83)
    }
    
    func testCalculateDistance2() {
        let start = MapPoint(x: 40.846268154160065, y: 14.268908911060743)
        let end = MapPoint(x: 40.834078077337125, y: 14.24113691165681)
        let distance = ArialDistanceCalculator.distanceInKmBetweenEarthCoordinates(start: start, end: end).rounded(toPlaces: 2)
        
        XCTAssertLessThan(distance, 2.71)
    }
    
}
