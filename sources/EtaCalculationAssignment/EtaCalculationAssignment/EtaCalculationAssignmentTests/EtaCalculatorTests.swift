//
//  EtaCalculatorTests.swift
//  EtaCalculationAssignmentTests
//
//  Created by Dmitry Mazo on 8/27/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import XCTest
@testable
import EtaCalculationAssignment

final class EtaCalculatorTests: XCTestCase {

    func test1Km() {
        let distanceInKm = 1.0
        let etaHours = EtaCalculator.eta(distanceInKm: distanceInKm).rounded(toPlaces: 3)
        
        XCTAssertEqual(etaHours, 0.028)
    }
    
    func test2000Km() {
        let distanceInKm = 2000.0
        let etaHours = EtaCalculator.eta(distanceInKm: distanceInKm).rounded(toPlaces: 3)
        
        XCTAssertEqual(etaHours, 55.556)
    }

}
