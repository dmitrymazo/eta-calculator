//
//  AddressViewModelCreatorTests.swift
//  EtaCalculationAssignmentTests
//
//  Created by Dmitry Mazo on 8/28/21.
//  Copyright © 2021 Company Inc. All rights reserved.
//

import XCTest
@testable
import EtaCalculationAssignment

final class AddressViewModelCreatorTests: XCTestCase {
    
    func testViewModel() {
        let addresses = [
            Address(address: "",
                    coordinates: MapPoint(x: 41, y: 12)),
            Address(address: "",
                    coordinates: MapPoint(x: 42, y: 15)),
            Address(address: "",
                    coordinates: MapPoint(x: 50, y: 20))
        ]
        
        let viewModels = AddressViewModelCreator.addressViewModels(from: addresses)
        
        XCTAssertEqual(viewModels[0].address, addresses[0])
        XCTAssertEqual(viewModels[1].address, addresses[1])
        XCTAssertEqual(viewModels[2].address, addresses[2])
        
        let secondToThirdAddressDistance = ArialDistanceCalculator.distanceInKmBetweenEarthCoordinates(start: addresses[1].coordinates, end: addresses[2].coordinates)
        let secondToThirdEta = EtaCalculator.eta(distanceInKm: secondToThirdAddressDistance)
        let correctAnswer = secondToThirdEta + viewModels[0].eta + viewModels[1].eta
        
        XCTAssertEqual(viewModels[2].eta, correctAnswer)
    }
    
}
