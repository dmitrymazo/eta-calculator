# ETA Calculator

## INSTALLATION

### Install xcodegen
Terminal:
``` mint install yonaskolb/xcodegen ```
OR
``` brew install xcodegen ```

### Generate the project file:
Get to the location of the project.yml file by calling:
``` cd /<project_location>/EtaCalculationAssignment/sources/EtaCalculationAssignment/EtaCalculationAssignment ```

Then call:
``` xcodegen generate ```

If everything is OK there will be a message in green:
``` Created project at <location> ```

## Notes
In the `SearchAddressViewController` you can specify whether you want to use the real API
 or a fake one by using either `WebGeolocationService` or `FakeGeolocationService` (real APIs usually have some request frequency limitations).